import aiosqlite
import logging
import os

logger = logging.getLogger(__name__)


async def init(app):
    db_path = app['cfg'].db_path
    
    if not os.path.exists(db_path):
        await create_database(db_path)
        
    logger.info("Connecting to database...")
    db = await aiosqlite.connect(db_path)
    logger.info("Database connected.")

    app['DB'] = db
    yield
    await db.close()
    
    
async def create_database(db_path):
    logger.info("Database does not exists. Creating...")
    
    with open(db_path, 'w'):
        pass
    
    create_tables_query = """
        CREATE TABLE url (
        id VARCHAR PRIMARY KEY UNIQUE NOT NULL,
        full_url TEXT NOT NULL
        );
    """
    
    async with aiosqlite.connect(db_path) as db:
        await db.execute(create_tables_query)
        await db.commit()
        
    logger.info("Database created")
    
    
async def get_by_id(db: aiosqlite.Connection, id: str) -> tuple | None:
    async with db.execute("SELECT * FROM url WHERE id = $1;", [id]) as cur:
        row = await cur.fetchone()
    return row


async def create(db: aiosqlite.Connection, key: str, full_url: str) -> None:
    await db.execute("INSERT INTO url (id, full_url) VALUES ($1, $2);", [key, full_url])
    await db.commit()
    
    
async def get_all(db: aiosqlite.Connection) -> list[dict]:
    result = []
    async with db.execute('SELECT * FROM url;') as cur:
        rows = await cur.fetchall()
        result.extend({"short_url": row[0], "full_url": row[1]} for row in rows)
    return result


async def delete(db: aiosqlite.Connection, key: str) -> None:
    await db.execute("DELETE FROM url WHERE id=$1;", [key])
    await db.commit()
