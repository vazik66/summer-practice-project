from aiohttp import web
from settings import Settings
from routes import routes
from database import init
import logging

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    app = web.Application()
    
    app['cfg'] = Settings()
    app.add_routes(routes)
    app.cleanup_ctx.append(init)
    
    web.run_app(app, host=app['cfg'].host, port=app['cfg'].port)
