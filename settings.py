class Settings:
    host: str = 'localhost'
    port: int = 8080
    db_path: str = 'database.db'
