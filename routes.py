from asyncio.log import logger
import logging
from aiohttp import web
import database
from secrets import token_urlsafe
from validators import url as is_url

routes = web.RouteTableDef()
logger = logging.getLogger(__name__)

@routes.get('/')
async def get_all(request: web.Request) -> web.Response:
    db = request.app['DB']
    all_links = await database.get_all(db)
    return web.json_response(all_links)


@routes.get('/{id}')
async def redirect(request: web.Request) -> web.Response:
    short_url = request.match_info['id']
    db = request.app['DB']
    
    row = await database.get_by_id(db, short_url)
    if not row:
        raise web.HTTPNoContent
        
    return web.HTTPFound(location=row[1])
        
        
@routes.post('/')
async def create(request: web.Request) -> web.Response:
    data = await request.json()
    
    full_url = data['url']
    if not is_url(full_url):
        raise web.HTTPBadRequest(reason="URL is invalid")
        
    db = request.app['DB']
    cfg = request.app['cfg']
    key = token_urlsafe(5)

    while row := await database.get_by_id(db, key):
        key = token_urlsafe(5)
    await database.create(db, key, full_url)

    short_url = f'{cfg.host}:{cfg.port}/{key}'
    return web.json_response({'short_url': short_url})


@routes.delete('/{id}')
async def delete(request: web.Request) -> web.Response:
    short_url = request.match_info['id']
    db = request.app['DB']

    row = await database.get_by_id(db, short_url)
    if not row:
        raise web.HTTPNoContent
    await database.delete(db, short_url)
        
    return web.HTTPNoContent(body='Deleted')